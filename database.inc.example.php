<?php
	if (__FILE__ == $_SERVER["SCRIPT_FILENAME"]) exit("No direct access allowed.");

	$db = @new mysqli("localhost", "USERNAME", "PASSWORD", "DATABASE");

	if ($db->connect_errno) {
		echo 'Die Datenbank konnte nicht erreicht werden. Folgender Fehler trat auf: '.mysqli_connect_errno(). ' : ' .mysqli_connect_error();
		exit;
	}
?>
