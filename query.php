<?php
header('Cache-Control: no-cache, must-revalidate');
header('Content-type: application/json');

require_once("database.inc.php");

$id = $db->real_escape_string($_GET["id"]);
if(isset($id)) {
	$data = $db->query("SELECT example FROM testdata WHERE id = $id LIMIT 1");
	if ($data->num_rows == 1) {
		$row = $data->fetch_object();
		echo json_encode(array("success"=>true, "example"=>$row->example));
	}
}
?>
